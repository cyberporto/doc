# Git log
alias glog='git log --stat --decorate'; export glog
alias gloga='git log --graph --abbrev-commit --decorate --date=relative --all'; export gloga

# When dealing with CVS in ports
export CVSROOT="anoncvs@anoncvs.fr.openbsd.org:/cvs"
