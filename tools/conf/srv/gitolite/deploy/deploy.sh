#!/bin/sh

# origin package directory
packages_dir="/srv/gitolite/deploy/packages"
# temporary work directory
deploy_dir="/srv/gitolite/deploy/deploy_dir"
# scripts to deploy packages
deploy_scripts="/srv/gitolite/deploy/scripts"

get_script(){
	# receives package path return script to call
	local pkg_path=$1
	echo $(head -2 ${pkg_path}/project | tail -1)
}

get_new(){
	# receives package path return commit hash (new)
	local pkg_path=$1
	echo $(head -3 ${pkg_path}/project | tail -1)
}

get_dep(){
	# receives package path return previews commit hash (old)
	local pkg_path=$1

	new=$(head -3 ${pkg_path}/project | tail -1)
	old=$(head -4 ${pkg_path}/project | tail -1)

	if [[ ! ${new} = ${old} ]]; then
		echo ${old} | cut -c1-7
	fi
}

project_extract(){

    # project directory containing extracted packages
    local prj_dir=$1

    # final extracted package
    local prj_pkg="${prj_dir}/package"

    # temporary vars for swapping/iterating pkg_news
    local pkg_new=""
    local pkg_old=""
    local pkg_dir=""
    local pkg_temp=""
    local pkg_next=1
    local pkg_del=""
    local x=0
    local y=0

    # array with all the news hashes
    #local pkg_news=($(ls ${prj_dir}))
    local pkg_news=$(ls ${prj_dir})

    # total new packages
    local total=${#pkg_news[*]}

    echo "Deploy: $(basename ${prj_dir}) extracting packages ${pkg_news[*]}"

    # find first package
    for pkg_new in ${pkg_news[*]}
    do
	    # get package dependency
	    pkg_dir="${prj_dir}/${pkg_new}"
	    pkg_old=$(get_dep ${pkg_dir})
	    #if [[ ! " $pkg_news[@] " =~ " ${pkg_old} " ]]; then
	    if [[ `echo $pkg_news[@] | grep -c \'${pkg_old}\'` -eq 1 ]]; then 
		    # pkg_news don't contain package
		    # we found initial package
		    pkg_temp=${pkg_news[0]}
		    pkg_news[0]=${pkg_new}
		    pkg_news[${x}]=${pkg_temp}
		    break
	    fi
	    x=$((${x}+1))
    done

    # Order packages by dependency start with first package
    #for (( y=0; y<${total}; y++ ))
    y=0
    while [ $y -lt $total ]
    do
	    pkg_next=$(($y+1))
	    if [[ ${pkg_next} = ${total} ]]; then
		    ## we are in the last one
		    break
	    fi

	    pkg_new=${pkg_news[$y]}
	    #for (( x=pkg_next; x<${total}; x++ ))
	    x=$pkg_next;
	    while [ $x -lt $total ]
	    do
		    pkg_dir="${prj_dir}/${pkg_news[${x}]}"
		    pkg_old=$(get_dep ${pkg_dir})
		    # is dependent on current
		    if [[ ${pkg_old} = ${pkg_new} ]]; then
			    pkg_temp=${pkg_news[${pkg_next}]}
			    pkg_news[${pkg_next}]=${pkg_news[${x}]}
			    pkg_news[${x}]=${pkg_temp}
			    # we can break and pass next one
			    break
		    fi
		    x=$x+1
	    done
	    y=$y+1
    done
    # create project final package directory
    mkdir -p ${prj_pkg}/files

    # copy project information of last commit
    cp ${prj_dir}/${pkg_news[$((${total}-1))]}/project ${prj_pkg}

    # now that packages are ordered we can start creating files
    for pkg_new in ${pkg_news[@]}
    do
	    pkg_dir=${prj_dir}/${pkg_new}
	    tar -C ${prj_pkg}/files -xf ${pkg_dir}/files.tar.xz
	    # if deleted files exists
	    if [ -f "${pkg_dir}/deleted" ]; then
		    # first collect all files/directories don't exist
		    while read pkg_del; do
			    # if file don't exist add entry to project deleted file
			    pkg_temp="${prj_pkg}/files/${pkg_del}"
			    if [ ! -f ${pkg_temp} ]; then
				    if [ ! -d ${pkg_temp} ]; then
					    # is not a file or directory from previous packages
					    echo ${pkg_del} >> ${prj_pkg}/deleted
				    fi
			    fi
		    done <${prj_dir}/${pkg_new}/deleted

	    # delete directories and files
	    while read pkg_del; do
		    pkg_temp="${prj_pkg}/files/${pkg_del}"
		    if [ -d ${pkg_temp} ]; then
			    rm -r ${pkg_temp}
		    elif [ -f ${pkg_temp} ]; then
			    rm ${pkg_temp}
		    fi
	    done <${prj_dir}/${pkg_new}/deleted
	    fi

	#remove temporary directory
	rm -r ${prj_dir}/${pkg_new}
done

    # call project deploy script
    call_script=${deploy_scripts}/$(get_script $prj_pkg)
    echo "Deploy: calling deploy script: ${call_script}"
    /bin/sh ${call_script} ${prj_pkg}

}

if [[ ! $(ls ${deploy_dir}) = "" ]]; then
	echo "removing deploy dir"
	rm -r ${deploy_dir}/*
fi

# first extract all packages from origin directory
for pkg_path in `find ${packages_dir} -type f -name "*.tar.gz"`
do
	if [ -f ${pkg_path} ]; then
		pkg_name=$(basename ${pkg_path})
		pkg_proj=$(echo ${pkg_name} | cut -d "_" -f 1)
		pkg_new7=$(echo ${pkg_name} | tail -c -15 | cut -c -7)
		pkg_temp=${deploy_dir}/${pkg_proj}/${pkg_new7}
		mkdir -p ${pkg_temp}
		tar -C ${pkg_temp} -zxf ${pkg_path} 
		rm ${pkg_path}
	fi
done

# loop for all projects and deploy them
for prj_dir in `find ${deploy_dir} -maxdepth 1 -mindepth 1 -type d`
do
	# order index of hashes based on old commit
	echo "Deploy: prj_dir=$prj_dir"
	project_extract ${prj_dir}
done
