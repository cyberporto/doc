#!/bin/sh

# final packages dir
packages_dir="/srv/gitolite/deploy/packages"
# hook work directory
hook_dir="/srv/gitolite/deploy/hook_dir"

is_initial(){
    local prj_name=$1
    if [ ! -d ${hook_dir}/${prj_name} ]; then
        echo "true"
    else
        echo "false"
    fi
}

get_remote_rev(){
    echo $(wget --no-check-certificate -qO- $1)
}

valid_url(){
    if [[ `wget -S --spider $1 --no-check-certificate 2>&1 | grep -c 'HTTP/1.1 200 OK'` -eq 1 ]];
    then
        echo "true";
    fi
}

create_package(){
    # project name
    local prj_name=$1
    # git repository directory
    local git_dir=$2
    # last/old commit revision
    local pkg_old=$3
    # new commit revision
    local pkg_new=$4
    # script deploy call when extracting this package
    local pkg_script=$5

    local pkg_new7=$(echo $pkg_new | cut -c1-7)

    # project directory
    local prj_dir="${hook_dir}/${prj_name}"
    # package directory
    local pkg_dir="${prj_dir}/${pkg_new7}"
    # final tar file
    local pkg_tar="${packages_dir}/${prj_name}_${pkg_new7}.tar.gz"

    # if temporary work directory exists maybe other process is creating packages
    if [ -d "$pkg_dir" ]; then
        echo "Deploy: temporary directory ${pkg_dir} exists, maybe other precess"
        exit 1
    fi
    # create temporary directory for this package
    mkdir -p ${pkg_dir}

    echo "Deploy: ${prj_name} ${pkg_new7} package call ${pkg_script} on deploy."

    # save metadata to be used by deploy script
    echo $prj_name > ${pkg_dir}/project
    echo $pkg_script >> ${pkg_dir}/project
    echo $pkg_new >> ${pkg_dir}/project

    # if is a valid old commit create a package with changes since then
    # else create a full package (all files)
    local is_commit=$(git --git-dir=${git_dir} cat-file -t ${pkg_old} 2>&1)
    if [[ $is_commit = "commit" ]]; then
        echo "Deploy: creating package from old commit."
        # list with files to extract (Added Copied Modified Renamed)
        file_list=$(git --git-dir=${git_dir} --no-pager diff \
            --diff-filter=ACMR \
            --name-only ${pkg_old} ${pkg_new})

        # create tar archive with same name as commit hash with files
        git --git-dir=${git_dir} archive -o ${pkg_dir}/files.tar.xz ${pkg_new} ${file_list}

        # first we create list of files to be removed
        git --git-dir=${git_dir} --no-pager diff \
            --diff-filter=DR \
            --name-status -t ${pkg_old} ${pkg_new} | cut -f 2 > ${pkg_dir}/deleted

        # save old commit on metadata
        echo $pkg_old >> ${pkg_dir}/project
    else
        echo "Deploy: creating initial package."
        git --git-dir=${git_dir} archive -o ${pkg_dir}/files.tar.xz ${pkg_new}

    fi

    tar -zcpf ${pkg_tar} -C ${pkg_dir} .

    echo "Deploy: package ${pkg_tar} ready !"
    rm -r ${pkg_dir}
    return 0
}
