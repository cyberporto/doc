check_git() {
    # check git configuration, returns
    # 0 - git command and repository exists
    # 1 - git command not found
    # 2 - git command found but rep don't
    if [ -x "$(command -v git)" ]; then
        if [ -f ${HOME}/doc/.git/config ]; then
            return 0;
        fi
        return 2;
    fi
    return 1;
}

check_ssh() {
    # check ssh client configuration, returns
    # 0 - ssh command and keys found
    # 1 - ssh command not found
    # 2 - ssh command found but no keys
    if [ -x "$(command -v ssh)" ]; then
        if [ -f ${HOME}/.ssh/$USER ]; then
            return 0;
        fi
        return 2;
    fi
    return 1;
}

check_tmux() {
    # check for tmux configuration, returns
    # 0 - tmux command and config found
    # 1 - tmux command not found
    # 2 - tmux command found without config
    if [ -x "$(command -v tmux)" ]; then
        if [ -f ${HOME}/.tmux.conf ]; then
            return 0;
        fi
        return 2;
    fi
    return 1
}
