#!/bin/sh

# First we define the function
ConfirmOrExit ()
{
    while true
    do
        echo -n "Please confirm (y or n) :"
        read CONFIRM
        case $CONFIRM in
            y|Y|YES|yes|Yes) break ;;
            n|N|no|NO|No)
                echo "Aborting - you entered $CONFIRM"
                exit
                ;;
            *) echo "Please enter only y or n"
        esac
    done
    echo "You entered $CONFIRM. Continuing ..."
}


# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

DIR=$(dirname "$SCRIPTPATH");
DIR_CONF=$DIR"/conf"

echo "SCRIPT=$SCRIPT";
echo "SCRIPTPATH=$SCRIPTPATH";
echo "DIR=$DIR";
echo "DIR_CONF=$DIR_CONF";
ConfirmOrExit

IS_INSTALL=$(prt-get isinst postgresql);
echo $IS_INSTALL;
if [ "$IS_INSTALL" = "package postgresql is installed" ]
then
    echo "updating postgresql"
    OLD_VERSION=$(prt-get current postgresql);
    echo $OLD_VERSION;

    sudo -u postgres pg_dumpall > /srv/pgsql/dump-$OLD_VERSION.sql

    sh /etc/rc.d/postgresql stop

    #extra backup, in case ...
    tar --xattrs -zcpf /srv/pgsql/data-$OLD_VERSION.tar.gz \
                    --directory=/srv/pgsql/data .

    rm -R /srv/pgsql/data

    prt-get update postgresql

    NEW_VERSION=$(prt-get current postgresql);
    echo $NEW_VERSION;

    sudo -u postgres initdb -D /srv/pgsql/data

    rejmerge
    #installer overwrite system init script
    cp -R $DIR_CONF/etc/rc.d/postgresql /etc/rc.d/

    sh /etc/rc.d/postgresql start
    sleep 5

    sudo -u postgres psql -d postgres -f /srv/pgsql/dump-$OLD_VERSION.sql

else
    echo "install postgresql and dependencies"
    prt-get depinst postgresql

    cp -R $DIR_CONF/etc/rc.d/postgresql /etc/rc.d/

    mkdir /srv/pgsql/
    touch /var/log/postgresql
    chown postgres:postgres /srv/pgsql /var/log/postgresql

    sudo -u postgres initdb -D /srv/pgsql/data

    cp $DIR_CONF/srv/pgsql/data/pg_hba.conf /srv/pgsql/data/
    chown postgres:postgres /srv/pgsql/data/pg_hba.conf
fi

exit 0;
