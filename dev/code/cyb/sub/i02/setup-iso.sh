#!/bin/sh

# location of iso and md5 file
ISO_DIR="/usr/ports/iso"
MOUNT_POINT="/mnt/media"

ISO_FILE="${ISO_DIR}/crux-3.6.iso"
MD5_FILE="${ISO_DIR}/crux-3.6.md5"

# iso and md5 remote location
#ISO_URL="https://serverop.de/crux/crux-3.6/iso/crux-3.6.iso"
ISO_URL="https://ftp.spline.inf.fu-berlin.de/pub/crux/crux-3.6/iso/crux-3.6.iso"
MD5_URL="https://serverop.de/crux/crux-3.6/iso/crux-3.6.md5"

# First we define the function
ConfirmOrExit () {
    while true
    do
        echo -n "Please confirm (y or n) :"
        read CONFIRM
        case $CONFIRM in
            y|Y|YES|yes|Yes) break ;;
            n|N|no|NO|No)
                echo "Aborting - you entered $CONFIRM"
                exit
                ;;
            *) echo "Please enter only y or n"
        esac
    done
    echo "You entered $CONFIRM. Continuing ..."
}

download_iso() {

    mkdir -p ${ISO_DIR}

    if [ -f $ISO_FILE ];
    then
        echo "File $ISO_FILE exists."
    else
        echo "File $ISO_FILE does not exist."
        cd $ISO_DIR && { curl -k -O $ISO_URL ; cd -; }
    fi

    if [ -f $MD5_FILE ];
    then
        echo "File ${MD5_FILE} exists."
    else
        echo "File ${MD5_FILE} does not exist."
        cd ${ISO_DIR} && { curl -k -O ${MD5_URL} ; cd -; }
    fi

}

check_iso() {
    if  cd ${ISO_DIR} && md5sum -c ${MD5_FILE} ;
    then
        echo "Valid iso md5sum"
    else
        echo "Invalid iso md5sum"
    fi
}

mount_iso() {

    if [ ! -f $ISO_FILE ];
    then
        echo "File $ISO_FILE does not exist."
        exit 0
    fi

    modprobe isofs
    modprobe loop
    mount -o loop $ISO_FILE $MOUNT_POINT
}

print_data() {
    echo "1.1.1 Paths to iso and md5 files:"
    echo "iso dir: ${ISO_DIR}"
    echo "iso file: ${ISO_FILE}"
    echo "md5 file: ${MD5_FILE}"
    echo "iso url: ${ISO_URL}"
    echo "md5 url: ${MD5_URL}"
    echo "mount point: ${MOUNT_POINT}"
}

print_help() {
	echo "usage: setup-iso [options]"
	echo "options:"
	echo "  -r,   --root                default dir is /usr/ports/iso"
	echo "  -d,   --download            download iso"
	echo "  -c,   --check               check iso md5sum"
	echo "  -m,   --mount               mount iso on /media "
	echo "  -h,   --help                print help and exit"
}

while [ "$1" ]; do
            case $1 in
                    -r|--root)
                        ISO_DIR=$2

                        ISO_FILE="${ISO_DIR}/crux-3.6.iso"
                        MD5_FILE="${ISO_DIR}/crux-3.6.md5"

                        shift ;;
                    -d|--download)
                        echo "Download iso:"
                        echo "_____________________"
                        print_data
                        ConfirmOrExit
                        download_iso
                        exit 0 ;;
                    -c|--check)
                        echo "Check iso md5sum:"
                        echo "_____________________"
                        print_data
                        ConfirmOrExit
                        check_iso
                        exit 0 ;;
                   -m|--mount)
                        echo "Check iso md5sum:"
                        echo "_____________________"
                        if [ ! -z "$2" ];
                        then
                            MOUNT_POINT=$2
                        fi
                        print_data
                        ConfirmOrExit
                        mount_iso
                        exit 0 ;;
                    -h|--help)
                        print_help
                        exit 0 ;;
                    *)
                        echo "setup-iso: invalid option $1"
                        print_help
                        exit 1 ;;
            esac
            shift
done

echo "setup-iso: no option provided"
print_help
exit 1
