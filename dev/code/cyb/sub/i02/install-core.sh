#!/bin/bash

CHROOT="/mnt"
PORT_PKG="${CHROOT}/media/crux"
INSTALL_OPT=0

#echo "1.1.8. Install Handbook\n"
#cp ${PORT_PKG}/handbook.txt $CHROOT/root/

ConfirmOrExit()
{
    while true
    do
        echo -n "Please confirm (y or n) :"
        read CONFIRM
        case $CONFIRM in
            y|Y|YES|yes|Yes) break ;;
            n|N|no|NO|No)
                echo "Aborting - you entered $CONFIRM"
                exit
                ;;
            *) echo "Please enter only y or n"
        esac
    done
    echo "You entered $CONFIRM. Continuing ..."
}


install_core() {

    mkdir -p ${CHROOT}/var/lib/pkg
    touch ${CHROOT}/var/lib/pkg/db

    CORE_LS="${DATA_DIR}/core.pkg"

    if [ ! -f ${CORE_LS} ]; then
        CORE_LS=${CHROOT}/core.pkg
        echo "1.1.4 Create core.pkg and install pkgadd"
        for p in ${PORT_PKG}/core/*;
        do
            echo $(basename $p) >> ${CORE_LS};
        done
    fi

    # it is required a degree in astrophysics in order to figure out how to extract a file to a target folder
    bsdtar -C ${CHROOT} -xf "${PORT_PKG}/core/pkgutils#5.40.7-1.pkg.tar.xz" usr/bin/pkgadd
    mv ${CHROOT}/usr/bin/pkgadd ${CHROOT}
    chmod +x ${CHROOT}/pkgadd

    echo "1.1.4 File core.pkg complete, review list of packages before continue... [PRESS ENTER]"
    read PAUSE
    vim ${CORE_LS}

    echo "1.1.4 Starting install"

    #cd $CHROOT
    while read line; do
        pkg=${PORT_PKG}/core/${line}
        echo "Installing ${pkg}"
        #${CHROOT}/pkgadd -f -r ${CHROOT} ${pkg}
        ${CHROOT}/pkgadd -f -r ${CHROOT} ${pkg}
    done < ${CORE_LS}

    rm ${CHROOT}/pkgadd

    if [ -f ${CHROOT}/core.pkg ]; then
        rm ${CHROOT}/core.pkg;
    fi
}

install_packages() {
    echo "Installing ${PORT_PKG}/opt/fakeroot"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/fakeroot#*
    echo "Installing ${PORT_PKG}/opt/dbus"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/dbus#*
    echo "Installing ${PORT_PKG}/opt/expat"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/expat#*
    echo "Installing ${PORT_PKG}/opt/libnl"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/libnl#*
    echo "Installing ${PORT_PKG}/opt/libpng"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/libpng#*
    echo "Installing ${PORT_PKG}/opt/freetype"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/freetype#*
    echo "Installing ${PORT_PKG}/opt/libffi"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/libffi#*
    echo "Installing ${PORT_PKG}/opt/sqlite3"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/sqlite3#*
    echo "Installing ${PORT_PKG}/opt/python"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/python#*
    echo "Installing ${PORT_PKG}/opt/glib"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/glib#*
    echo "Installing ${PORT_PKG}/opt/grub2"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/grub2#*
    echo "Installing ${PORT_PKG}/opt/grub2-efi"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/grub2-efi#*
    echo "Installing ${PORT_PKG}/opt/wireless-tools"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/wireless-tools#*
    echo "Installing ${PORT_PKG}/opt/wpa_supplicant"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/wpa_supplicant#*
    echo "Installing ${PORT_PKG}/opt/lvm2"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/lvm2#*
    echo "Installing ${PORT_PKG}/opt/mdadm"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/mdadm#*
    echo "Installing ${PORT_PKG}/opt/efivar"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/efivar#*
    echo "Installing ${PORT_PKG}/opt/efibootmgr"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/efibootmgr#*
    echo "Installing ${PORT_PKG}/opt/dosfstools"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/dosfstools#*
    echo "Installing ${PORT_PKG}/opt/parted"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/parted#*
    echo "Installing ${PORT_PKG}/opt/libgcrypt"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/libgcrypt#*
    echo "Installing ${PORT_PKG}/opt/cryptsetup"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/cryptsetup#*
    echo "Installing ${PORT_PKG}/opt/popt"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/popt#*
    echo "Installing ${PORT_PKG}/opt/libgpg-error"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/libgpg-error#*
    echo "Installing ${PORT_PKG}/opt/libevent"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/libevent#*
    echo "Installing ${PORT_PKG}/opt/libtirpc"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/libtirpc#*
    echo "Installing ${PORT_PKG}/opt/git"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/git#*
    echo "Installing ${PORT_PKG}/opt/tmux"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/tmux#*
    echo "Installing ${PORT_PKG}/opt/prt-utils"
    $CHROOT/usr/bin/pkgadd -f -r $CHROOT ${PORT_PKG}/opt/prt-utils#*
}

print_data() {
    echo "1.1.4 Paths to packages and target:"
    echo "chroot: ${CHROOT}"
    echo "packages dir: ${PORT_PKG}"
    echo "install optional: ${INSTALL_OPT}"
}

print_help() {
	echo "usage: install-core [options]"
	echo "options:"
	echo "  -r,   --chroot              default chroot is /mnt"
	echo "  -p,   --packages            packages directory /mnt/media/crux"
	echo "  -o,   --optional            install extra packages"
	echo "  -h,   --help                print help and exit"
}

while [ "$1" ]; do
            case $1 in
                    -r|--chroot)
                        CHROOT=$2
                        shift ;;
                    -p|--packages)
                        PORT_PKG=$2
                        shift ;;
                    -o|--optional)
                        INSTALL_OPT=1
                        ;;
                    -h|--help)
                        print_help
                        exit 0 ;;
                    *)
                        echo "setup-iso: invalid option $1"
                        print_help
                        exit 1 ;;
            esac
            shift
done

print_data
ConfirmOrExit
install_core
echo "=========[ core installed ]==========="

if [[ $INSTALL_OPT -eq 1 ]];
then
    install_packages
    echo "=========[ extra packages installed ]==========="
fi
