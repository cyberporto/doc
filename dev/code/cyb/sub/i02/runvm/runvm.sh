#!/bin/bash

function rmac_addr (){
printf '54:60:BE:EF:%02X:%02X\n' $((RANDOM%256)) $((RANDOM%256))
}

source profile/$1

#mac=$(rmac_addr)
#memory=1024
#boot=d
#tap="tap1"
#iso=iso/crux-3.4.iso
#image=img/crux-standard.qcow2
#other="-soundhw hda -vga std -display sdl"

qemu-system-x86_64 \
    -enable-kvm \
    -m ${memory} \
    -boot ${boot} \
    -cdrom ${iso} \
    -hda ${image} \
    -device e1000,netdev=t0,mac=${mac} \
    -netdev tap,id=t0,ifname=${tap},script=no,downscript=no \
    ${other} \
    &
