#!/bin/sh

# First we define the function
ConfirmOrExit ()
{
    while true
    do
        echo -n "Please confirm (y or n) :"
        read CONFIRM
        case $CONFIRM in
            y|Y|YES|yes|Yes) break ;;
            n|N|no|NO|No)
                echo "Aborting - you entered $CONFIRM"
                exit
                ;;
            *) echo "Please enter only y or n"
        esac
    done
    echo "You entered $CONFIRM. Continuing ..."
}


# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

DIR=$(dirname "$SCRIPTPATH");
DIR_CONF=$DIR"/conf"

echo "SCRIPT=$SCRIPT";
echo "SCRIPTPATH=$SCRIPTPATH";
echo "DIR=$DIR";
echo "DIR_CONF=$DIR_CONF";
ConfirmOrExit

    prt-get depinst gitolite

    mkdir -p /srv/gitolite
    useradd -U -d /srv/gitolite gitolite
    chown gitolite:gitolite /srv/gitolite

exit 0;
