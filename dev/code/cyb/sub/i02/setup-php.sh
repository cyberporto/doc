#!/bin/sh

. `dirname $0`/config-install.sh

prt-get depinst php php-fpm php-gd php-pdo-pgsql php-postgresql

cp /etc/php/php.ini-development /etc/php/php.ini
