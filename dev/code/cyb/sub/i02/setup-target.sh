#!/bin/sh

DEV=/dev/sda
VG=vg_system

SETUP_TARGET="print"
CHROOT="/mnt"

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

DIR=$(dirname "$SCRIPTPATH");
DIR_LOCAL="$(dirname $(dirname ${DIR}))/local";

##read BLK_EFI
BLK_EFI="${DEV}2"
##read BLK_BOOT
BLK_BOOT="${DEV}3"
##read BLK_ROOT
BLK_CRYPT="${DEV}4"
BLK_ROOT="/dev/$VG/lv_root"
##read BLK_VAR
BLK_VAR="/dev/${VG}/lv_var"
##read BLK_USR
#BLK_USR="${DEV}6"
##read BLK_SWP
BLK_SWP="/dev/${VG}/lv_swap"
##read BLK_HOME
BLK_HOME="/dev/${VG}/lv_home"


# First we define the function
ConfirmOrExit ()
{
    while true
    do
        echo -n "Please confirm (y or n) :"
        read CONFIRM
        case $CONFIRM in
            y|Y|YES|yes|Yes) break ;;
            n|N|no|NO|No)
                echo "Aborting - you entered $CONFIRM"
                exit
                ;;
            *) echo "Please enter only y or n"
        esac
    done
    echo "You entered $CONFIRM. Continuing ..."
}


partition_target () {

    parted --script $DEV \
        mklabel gpt \
        unit mib \
        mkpart primary 1 3 \
        name 1 grub \
        set 1 bios_grub on \
        mkpart ESP fat32 3 125 \
        name 2 efi \
        set 2 boot on \
        mkpart primary ext4 125 1128 \
        name 3 boot \
        mkpart primary 1128 100% \
        set 4 lvm on

    modprobe dm-crypt
    cryptsetup luksFormat ${BLK_CRYPT}
    cryptsetup luksOpen ${BLK_CRYPT} cryptlvm

    pvcreate /dev/mapper/cryptlvm
    vgcreate ${VG} /dev/mapper/cryptlvm

    lvcreate -L 20G -n lv_root ${VG}
    lvcreate -L 4G -n lv_var ${VG}
    lvcreate -L 8G -n lv_swap ${VG}
    lvcreate -L 120G -n lv_home ${VG}

}

mount_target () {
    echo "1.1.2 Creating File System on $BLK_EFI with fat32:"
    mkfs.fat -F 32 $BLK_EFI
    echo "1.1.2 Creating File System on $BLK_BOOT with ext4:"
    mkfs.ext4 $BLK_BOOT
    echo "1.1.2 Creating File System on $BLK_ROOT with ext4:"
    mkfs.ext4 $BLK_ROOT
    echo "1.1.2 Creating File System on $BLK_VAR with ext4:"
    mkfs.ext4 $BLK_VAR
    #echo "1.1.2 Creating File System on $BLK_USR with ext4:"
    #mkfs.ext4 $BLK_USR
    echo "1.1.2 Creating Swap File System on $BLK_SWP:"
    mkswap $BLK_SWP
    echo "1.1.2 Creating File System on $BLK_HOME with ext4:"
    mkfs.ext4 $BLK_HOME

    echo "1.1.3 mount point to chroot (/mnt):\n"
    mount $BLK_ROOT $CHROOT

    mkdir -p $CHROOT/boot
    mount $BLK_BOOT $CHROOT/boot

    mkdir -p $CHROOT/boot/efi
    mount $BLK_EFI $CHROOT/boot/efi

    mkdir -p $CHROOT/var
    mount $BLK_VAR $CHROOT/var

    #mkdir -p $CHROOT/usr
    #mount $BLK_USR $CHROOT/usr

    mkdir -p $CHROOT/home
    mount $BLK_HOME $CHROOT/home

    mkdir -p $CHROOT/var/lib/pkg
    mkdir -p $CHROOT/usr/ports

    mkdir -p $CHROOT/media

    mkdir -p $CHROOT/dev
    mkdir -p $CHROOT/tmp
    mkdir -p $CHROOT/proc
    mkdir -p $CHROOT/sys

}

directory_target () {

    mkdir -p $CHROOT/home
    mkdir -p $CHROOT/boot/efi
    mkdir -p $CHROOT/var/lib/pkg
    mkdir -p $CHROOT/usr/ports

    mkdir -p $CHROOT/media

    mkdir -p $CHROOT/dev
    mkdir -p $CHROOT/tmp
    mkdir -p $CHROOT/proc
    mkdir -p $CHROOT/sys

}


enable_target () {

    mount --bind /dev $CHROOT/dev
    mount -vt devpts devpts $CHROOT/dev/pts
    mount -vt tmpfs shm $CHROOT/dev/shm
    mount -vt proc proc $CHROOT/proc
    mount -vt sysfs sysfs $CHROOT/sys

}

print_target() {
    echo "Device: $DEV"
    echo "CHROOT: $CHROOT"
    echo "Option Selected: $SETUP_TARGET"

    echo "1.1.2 EFI block; ($BLK_EFI)"
    echo "1.1.2 boot block; ($BLK_BOOT)"
    echo "1.1.2 cryptlvm block; ($BLK_CRYPT)"
    echo "1.1.2 root block; ($BLK_ROOT)"
    echo "1.1.2 var block; ($BLK_VAR)"
    echo "1.1.2 usr block; ($BLK_USR)"
    echo "1.1.2 swap block; ($BLK_SWP)"
    echo "1.1.2 home block; ($BLK_HOME)"


}

print_help() {
	echo "usage: setup_target [options]"
	echo "options:"
	echo "  -p,   --partition           create partitions and file systems"
	echo "  -m,   --mount               mount partitions on chroot"
	echo "  -d,   --directory           keep temporary working directory"
        echo "  -e,   --enable              enable chroot (proc,dev, sys...)"
	echo "  -v,   --view                view environment vars and exit"
	echo "  -h,   --help                print help and exit"
}


while [ "$1" ]; do
            case $1 in
                    -p|--partition)
                            SETUP_TARGET="partition"
                            print_target
                            ConfirmOrExit
                            partition_target
                            exit 0 ;;
                    -m|--mount)
                            SETUP_TARGET="mount"
                            print_target
                            ConfirmOrExit
                            mount_target
                            exit 0 ;;
                    -d|--directory)
                            SETUP_TARGET="directory"
                            print_target
                            ConfirmOrExit
                            directory_target
                            exit 0 ;;
                    -e|--enable)
                            SETUP_TARGET="enable"
                            print_target
                            ConfirmOrExit
                            enable_target
                            exit 0 ;;
                    -v|--view)
                            SETUP_TARGET="view"
                            print_target
                            exit 0 ;;
                    -h|--help)
                            print_help
                            exit 0 ;;
                    *)
                            echo "setup-target: invalid option $1"
                            exit 1 ;;
            esac
            shift
done
