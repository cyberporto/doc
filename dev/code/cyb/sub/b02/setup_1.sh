#!/bin/sh

setup_1() {
    # setups tmux and returns;
    # 0 - on success
    # 1 - on failure
    if [ ! -x "$(command -v tmux)" ]; then
        echo "${RED}[NO] - you need to install tmux."
        echo $CLS
        return 1;
    fi

    if [ ! -f "${HOME}/.tmux.conf" ]; then
        if [ "$SYSTEM" = "OpenBSD" ]; then
            cp ${HOME}/doc/openbsd/conf/skel/.tmux.conf ${HOME}/
            echo "${GRE}[OK] - Copy to ~/.tmux.conf for OpenBSD done."
        fi
        cp ${HOME}/doc/linux/conf/skel/.tmux.conf ${HOME}/
        echo "${GRE}[OK] - Copy to ~/.tmux.conf for Linux done."
    fi

    echo "${GRE}[OK] - Tmux is ready."
    return 0;
}
