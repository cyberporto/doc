#!/bin/sh

setup_0() {
    # setups ssh client and returns;
    # 0 - on success
    # 1 - on failure
    if [ ! -x "$(command -v ssh)" ]; then
        echo "${RED}[NO] - you need to install ssh."
        echo $CLS
        return 1;
    fi

    if [ -d ${HOME}/.ssh ]; then
        echo "${GRE}[OK] - ssh directory exists"
    else
        echo "${RED}[NO] - ssh directory not found creating one.${GRE}"
        mkdir -p ${HOME}/.ssh
    fi

    if [ ! -d ${HOME}/.ssh ]; then
        echo "${RED}[NO] - failed to create ssh directory"
        return 1;
    fi

    chmod 700 ${HOME}/.ssh

    if [ ! -f ${HOME}/.ssh/authorized_keys ]; then
        touch ${HOME}/.ssh/authorized_keys
    fi
    chmod 600 ${HOME}/.ssh/authorized_keys
    echo "${GRE}[OK] - setting folder file modes"

    if [ -f ${HOME}/.ssh/$USER ]; then
        echo "[OK] - private key found"
    else
        echo "${RED}[NO] - private key not found"
        echo "${CLS}$ ssh-keygen -f ${HOME}/.ssh/${USER}"
        echo "${RED}[ATTENTION] - just press enter two times,${CLS}"
        echo "${RED}[ATTENTION] - empty for no passphrase${CLS}"
        ssh-keygen -f ${HOME}/.ssh/${USER}
    fi

    if [ ! -f ${HOME}/.ssh/$USER ]; then
        echo "${RED}[NO] - failed to create keys."
        return 1;
    fi

    chmod 600 ${HOME}/.ssh/${USER}

    echo "${GRE}[OK] - SSH is ready."
    echo $CLS
    return 0;
}
