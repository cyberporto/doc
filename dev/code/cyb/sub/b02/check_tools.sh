#!/bin/sh

check_vim() {
    # check vim configuration, returns
    # 0 - vim command and configuration exists
    # 1 - vim command not found
    # 2 - vim command found but config don't
    if [ -x "$(command -v vim)" ]; then
        if [ -f ${HOME}/.vimrc ]; then
            return 0;
        fi
        return 2;
    fi
    return 1;
}


