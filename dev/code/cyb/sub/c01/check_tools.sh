#!/bin/sh

check_httpd() {
    # check httpd configuration, returns
    # 0 - httpd command and configuration exists
    # 1 - httpd command not found
    # 2 - httpd command found but config don't
    if [ -x "$(command -v httpd)" ]; then
        if [ -f /etc/httpd.conf ]; then
            return 0;
        fi
        return 2;
    fi
    return 1;
}
