#!/bin/sh

setup_3() {
    # setups httpd and returns;
    # 0 - on success
    # 1 - on failure

    DOC=$1

    doas cp ${DOC}/openbsd/conf/httpd.conf /etc/httpd.conf
}
