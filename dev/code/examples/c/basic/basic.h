enum operations{deposit, redraw}

union u_account {
    int id;
    int value;
    char *client_name;
    int *log_head;
    union u_account *next;
}

struct s_accounts {
    int total_accounts;
    int total_value;
    union u_account *head_account;
}

struct s_operation {
    int time, amount;
    enum operations op;
    struct s_operation *next;
}

struct s_log {
    int number;
    struct s_operation *operation;
    struct s_log *next;
}
