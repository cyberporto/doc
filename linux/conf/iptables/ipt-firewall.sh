#!/bin/bash

. /etc/iptables/ipt-conf.sh

ipt_clear () {
    echo "clear all iptables tables"

    $IPT -F
    $IPT -X
    $IPT6 -F
    $IPT6 -X
    #$PIT4 -Z
    #$PIT6 -Z
    $IPT -t nat -F
    $IPT -t nat -X
    $IPT -t mangle -F
    $IPT -t mangle -X
    $IPT -t raw -F
    $IPT -t raw -X
    $IPT -t security -F
    $IPT -t security -X
    $IPT -N blocker
    $IPT -N blockip_in
    $IPT -N blockip_out

    $IPT -N srv_dhcp
    $IPT -N srv_rip
    $IPT -N srv_icmp
    $IPT -N srv_ntp
    $IPT -N srv_dns_in
    $IPT -N srv_dns_out
    $IPT -N srv_http_in
    $IPT -N srv_http_out
    $IPT -N srv_https_in
    $IPT -N srv_https_out
    $IPT -N srv_smtp_in
    $IPT -N srv_smtp_out
    $IPT -N srv_ssh_in
    $IPT -N srv_ssh_out
    $IPT -N srv_git_in
    $IPT -N srv_git_out
    $IPT -N srv_db_in
    $IPT -N srv_db_out


    $IPT -N cli_dns_in
    $IPT -N cli_dns_out
    $IPT -N cli_http_in
    $IPT -N cli_http_out
    $IPT -N cli_https_in
    $IPT -N cli_https_out
    $IPT -N cli_ssh_in
    $IPT -N cli_ssh_out
    $IPT -N cli_pops_in
    $IPT -N cli_pops_out
    $IPT -N cli_smtps_in
    $IPT -N cli_smtps_out
    $IPT -N cli_irc_in
    $IPT -N cli_irc_out
    $IPT -N cli_ftp_in
    $IPT -N cli_ftp_out
    $IPT -N cli_git_in
    $IPT -N cli_git_out
    $IPT -N cli_gpg_in
    $IPT -N cli_gpg_out

    # Set Default Rules
    $IPT -P INPUT DROP
    $IPT -P FORWARD DROP
    $IPT -P OUTPUT DROP

    # Set Default Rules
    $IPT6 -P INPUT DROP
    $IPT6 -P FORWARD DROP
    $IPT6 -P OUTPUT DROP

}

ipt_log () {
    ## log everything else and drop
    $IPT -A OUTPUT -j LOG --log-level 7 --log-prefix "iptables: OUTPUT: "
    $IPT -A INPUT -j LOG --log-level 7 --log-prefix "iptables: INPUT: "
    $IPT -A FORWARD -j LOG --log-level 7 --log-prefix "iptables: FORWARD: "

    $IPT6 -A OUTPUT -j LOG --log-level 7 --log-prefix "iptables: OUTPUT: "
    $IPT6 -A INPUT -j LOG --log-level 7 --log-prefix "iptables: INPUT: "
    $IPT6 -A FORWARD -j LOG --log-level 7 --log-prefix "iptables: FORWARD: "
}

ipt_tables () {
    echo "start adding tables..."
    # Filter out comments and blank lines
    # store each ip or subnet in $ip
    if [ -f ip_blocker ]
    then
        ./ip_blocker ${SPAMLIST}
    else
        grep -v '^#' < ${SPAMLIST} | { while read ip
        do
          # Append everything to droplist
          #echo "adding ${ip} to blockip"
          $IPT -A blockip_in -s $ip -j LOG --log-prefix "${SPAMDROPMSG}"
          $IPT -A blockip_in -s $ip -j DROP
          $IPT -A blockip_out -d $ip -j LOG --log-prefix "${SPAMDROPMSG}"
          $IPT -A blockip_out -d $ip -j DROP
        done; }
        echo "blockip_in and blockip_out added"
    fi

    ####### blocker Chain  ######
    ## Block google dns
    #$IPT -A blocker -s 8.8.0.0/24 -j LOG --log-level 7 --log-prefix "iptables: blocker google: "
    #$IPT -A blocker -s 8.8.0.0/24 -j DROP
    ## Block sync
    $IPT -A blocker -p tcp ! --syn -m state --state NEW -m limit --limit 5/m --limit-burst 7 -j LOG --log-level 7 --log-prefix "iptables: drop sync: "
    $IPT -A blocker -p tcp ! --syn -m state --state NEW -j DROP
    ## Block Fragments
    $IPT -A blocker -f -m limit --limit 5/m --limit-burst 7 -j LOG --log-level 4 --log-prefix "iptables: drop frag: "
    $IPT -A blocker -f -j DROP
    $IPT -A blocker -p tcp --tcp-flags ALL FIN,URG,PSH -j DROP
    $IPT -A blocker -p tcp --tcp-flags ALL ALL -j DROP
    $IPT -A blocker -p tcp --tcp-flags ALL NONE -m limit --limit 5/m --limit-burst 7 -j LOG --log-level 4 --log-prefix "iptables: drop null: "
    $IPT -A blocker -p tcp --tcp-flags ALL NONE -j DROP # NULL packets
    $IPT -A blocker -p tcp --tcp-flags SYN,RST SYN,RST -m limit --limit 5/m --limit-burst 7 -j LOG --log-level 4 --log-prefix "iptables: drop syn rst syn rst: "
    $IPT -A blocker -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
    $IPT -A blocker -p tcp --tcp-flags SYN,FIN SYN,FIN -m limit --limit 5/m --limit-burst 7 -j LOG --log-level 4 --log-prefix "iptables: drop xmas: "
    $IPT -A blocker -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP #XMAS
    $IPT -A blocker -p tcp --tcp-flags FIN,ACK FIN -m limit --limit 5/m --limit-burst 7 -j LOG --log-level 4 --log-prefix "iptables: drop fin scan: "
    $IPT -A blocker -p tcp --tcp-flags FIN,ACK FIN -j DROP # FIN packet scans
    $IPT -A blocker -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP
    #$IPT -A blocker -p tcp --tcp-flags ACK,FIN FIN -j DROP
    #$IPT -A blocker -p tcp --tcp-flags ACK,PSH PSH -j DROP
    #$IPT -A blocker -p tcp --tcp-flags ACK,URG URG -j DROP
    #$IPT -A blocker -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
    #$IPT -A blocker -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
    #$IPT -A blocker -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
    #$IPT -A blocker -p tcp --tcp-flags ALL ALL -j DROP
    #$IPT -A blocker -p tcp --tcp-flags ALL NONE -j DROP
    #$IPT -A blocker -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
    #$IPT -A blocker -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP
    #$IPT -A blocker -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP

    ## Return to caller
    $IPT -A blocker -j RETURN

    ######## DNS Server
    #echo "server_in chain: Allow input to DNS Server"
    $IPT -A srv_dns_in -p udp --dport 53 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A srv_dns_in -p tcp --dport 53 --sport 1024:65535  -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A srv_dns_in -j RETURN
    #echo "srv_dns_out chain: Allow output from DNS server"
    $IPT -A srv_dns_out -p udp --sport 53 --dport 1024:65535 -m state --state RELATED,ESTABLISHED -j ACCEPT
    $IPT -A srv_dns_out -p tcp --sport 53 --dport 1024:65535 -m state --state RELATED,ESTABLISHED -j ACCEPT
    $IPT -A srv_dns_out -j RETURN

    ####### Database Server
    $IPT -A srv_db_in -p tcp --dport 5432 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A srv_db_in -j RETURN
    $IPT -A srv_db_out -p tcp --sport 5432 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A srv_db_out -j RETURN

    ####### SSH Server

    $IPT -A srv_ssh_in -p tcp --dport 2222 -s ${BR_NET} -m state --state NEW -j ACCEPT
    $IPT -A srv_ssh_in -p tcp --dport 2222 -m state --state NEW -j LOG --log-prefix "iptables: SSH NEW":
    $IPT -A srv_ssh_in -p tcp --dport 2222 -m state --state NEW -m recent --set --name SSH -j ACCEPT

    $IPT -A srv_ssh_in -p tcp --dport 2222 -m recent \
        --update --seconds 60 --hitcount 4 --rttl \
        --name SSH -j LOG --log-prefix "${SPAMDROPMSG} SSH"

    $IPT -A srv_ssh_in -p tcp --dport 2222 -m recent --update --seconds 60 \
        --hitcount 4 --rttl --name SSH -j DROP

    $IPT -A srv_ssh_in -p tcp --dport 2222 --sport 1024:65535 -m state --state ESTABLISHED -j ACCEPT

    #$IPT -A srv_ssh_in -p tcp --dport 22 -m state --state NEW -m recent --set --name SSH -j ACCEPT

    #$IPT -A srv_ssh_in -p tcp --dport 22 -m recent \
    #    --update --seconds 60 --hitcount 4 --rttl \
    #    --name SSH -j LOG --log-prefix "${SPAMDROPMSG} SSH"

    #$IPT -A srv_ssh_in -p tcp --dport 22 -m recent --update --seconds 60 \
    #    --hitcount 4 --rttl --name SSH -j DROP

    #$IPT -A srv_ssh_in -p tcp --dport 22 --sport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A srv_ssh_in -j RETURN

    $IPT -A srv_ssh_out -p tcp --sport 2222 --dport 1024:65535 -d ${BR_NET} -m state --state ESTABLISHED -j ACCEPT
    $IPT -A srv_ssh_out -p tcp --tcp-flags SYN,ACK SYN,ACK --sport 2222 -j LOG --log-prefix "iptables: SSH OUT":
    $IPT -A srv_ssh_out -p tcp --sport 2222 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    #$IPT -A srv_ssh_out -p tcp --sport 22 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A srv_ssh_out -j RETURN

    ####### smtp Server
    $IPT -A srv_smtp_in -p tcp --dport 25 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A srv_smtp_in -j RETURN
    $IPT -A srv_smtp_out -p tcp --sport 25 --dport 1024:65535 -m state --state RELATED,ESTABLISHED -j ACCEPT
    $IPT -A srv_smtp_out -j RETURN

    ####### HTTP Server
    $IPT -A srv_http_in -p tcp --dport 80 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A srv_http_in -j RETURN
    $IPT -A srv_http_out -p tcp --sport 80 --dport 1024:65535 -m state --state RELATED,ESTABLISHED -j ACCEPT
    $IPT -A srv_http_out -j RETURN

    ####### HTTPS Server
    $IPT -A srv_https_in -p tcp --dport 443 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A srv_https_in -j RETURN
    $IPT -A srv_https_out -p tcp --sport 443 --dport 1024:65535 -m state --state RELATED,ESTABLISHED -j ACCEPT
    $IPT -A srv_https_out -j RETURN

    ###### GIT server
    $IPT -A srv_git_in -p tcp --dport 9418 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A srv_git_in -j RETURN
    $IPT -A srv_git_out -p tcp --sport 9418 --dport 1024:65535 -m state --state RELATED,ESTABLISHED -j ACCEPT
    $IPT -A srv_git_out -j RETURN

    ######## DNS Client
    $IPT -A cli_dns_out -p udp --dport 53 --sport 1024:65535 -j ACCEPT
    $IPT -A cli_dns_out -j RETURN
    $IPT -A cli_dns_in -p udp --sport 53 --dport 1024:65535 -j ACCEPT
    $IPT -A cli_dns_in -j RETURN

    ######## HTTP Client
    #$IPT -A cli_http_in -p tcp -m tcp --tcp-flags ACK --sport 80 --dport 1024:65535 -j DROP
    $IPT -A cli_http_in -p tcp --sport 80 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_http_in -p udp --sport 80 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_http_in -j RETURN
    $IPT -A cli_http_out -p tcp --dport 80 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_http_out -p udp --dport 80 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_http_out -j RETURN

    ######## IRC client
    $IPT -A cli_irc_in -p tcp --sport 6667 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_irc_in -j RETURN
    $IPT -A cli_irc_out -p tcp --dport 6667 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_irc_out -j RETURN

    ######## FTP client
    $IPT -A cli_ftp_in -p tcp --sport 21 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_ftp_in -p tcp --sport 20 --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT
    $IPT -A cli_ftp_in -p tcp --sport 1024:65535 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_ftp_in -j RETURN
    $IPT -A cli_ftp_out -p tcp --dport 21 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_ftp_out -p tcp --dport 20 --sport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_ftp_out -p tcp --sport 1024:65535 --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT
    $IPT -A cli_ftp_out -j RETURN

    ######## GIT client
    $IPT -A cli_git_in -p tcp --sport 873 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_git_in -p tcp --sport 9418 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_git_in -j RETURN
    $IPT -A cli_git_out -p tcp --dport 873 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_git_out -p tcp --dport 9418 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_git_out -j RETURN

    ######## POP3S client
    $IPT -A cli_pops_in -p tcp --sport 995 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_pops_in -j RETURN
    $IPT -A cli_pops_out -p tcp --dport 995 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_pops_out -j RETURN

    ######## SMTPS client
    $IPT -A cli_smtps_in -p tcp --sport 465 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_smtps_in -j RETURN
    $IPT -A cli_smtps_out -p tcp --dport 465 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_smtps_out -j RETURN

    ######## HTTPS client
    $IPT -A cli_https_in -p tcp --sport 443 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_https_in -p udp --sport 443 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_https_in -j RETURN
    $IPT -A cli_https_out -p tcp --dport 443 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_https_out -p udp --dport 443 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_https_out -j RETURN

    ######## SSH client
    $IPT -A cli_ssh_in -p tcp --sport 2222 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_ssh_in -p tcp --sport 22 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_ssh_in -j RETURN

    $IPT -A cli_ssh_out -p tcp -d ${BR_NET} --dport 2222 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_ssh_out -p tcp --tcp-flags SYN,ACK SYN,ACK --dport 2222 -j LOG --log-prefix "iptables: SSH OUT":
    $IPT -A cli_ssh_out -p tcp --dport 2222 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_ssh_out -p tcp --dport 22 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_ssh_out -j RETURN

    ######## GPG key client
    $IPT -A cli_gpg_in -p tcp --sport 11371 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    $IPT -A cli_gpg_in -j RETURN
    $IPT -A cli_gpg_out -p tcp --dport 11371 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
    $IPT -A cli_gpg_out -j RETURN

    ######## DHCP Server
    $IPT -A srv_dhcp -p udp --sport 68 --dport 67 -j ACCEPT
    $IPT -A srv_dhcp -p udp --sport 67 --dport 68 -j ACCEPT
    $IPT -A srv_dhcp -p udp --sport 67 --dport 67 -j ACCEPT
    $IPT -A srv_dhcp -j RETURN

    ####### RIP Server
    $IPT -A srv_rip -p udp --sport 520 --dport 520 -j ACCEPT
    $IPT -A srv_rip -j RETURN

    ####### ICMP Server
    $IPT -A srv_icmp -p icmp -j ACCEPT
    $IPT -A srv_icmp -j RETURN

    ####### NTP Client and Server
    $IPT -A srv_ntp -p udp --sport 123 --dport 123 -j ACCEPT
    $IPT -A srv_ntp -j RETURN

}
