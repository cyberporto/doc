# If not running interactively, don't do anything
case $- in
	*i*) ;;
	*) return;;
esac


# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize


# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000


alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ls='ls -ph --color=auto'
alias rm='rm -i'
#alias cp='cp -i'
alias mv='mv -i'
# Prevents accidentally clobbering files.
alias mkdir='mkdir -p'
alias h='history'
alias hg='history | grep'
alias j='jobs -l'
alias which='type -a'
alias ..='cd ..'

# Generate a password
genpasswd () {
    local l=$1
    [ "$l" == "" ] && l=20
    tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs
}

# Git log
glog () {
    git log --stat --decorate
}
# Git graph log
gloga () {
    git log --graph --abbrev-commit --decorate --date=relative --all
}

alias tmux="tmux -2"

# Virtual Crux machine
alias c1.ank="ssh c1 -t tmux a"
alias c2.ank="ssh c2 -t tmux a"
alias c9.ank="ssh c9 -t tmux a"

alias pkg_mirror="pkg_bin -f /usr/ports/mirror_bin_db"
alias pkg_update="pkg_bin -r /usr/ports/mirror_bin_db"

#if [[ -z "$TMUX" ]] ;then
#    ID="`tmux ls | grep -vm1 attached | cut -d: -f1`" # get the id of a deattached session
#    if [[ -z "$ID" ]] ;then # if not available create a new one
#        tmux new-session
#    else
#        tmux attach-session -t "$ID" # if available attach to it
#    fi
#fi
MAIL=/home/silvino/.mail/
